package main

import (
	"bytes"
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/http"

	"code.google.com/p/go.crypto/scrypt"
)

type Request struct {
	ID     int      `json:"id"`
	Method string   `json:"method"`
	Params []string `json:"params"`
}

type Response struct {
	ID     int         `json:"id"`
	Error  interface{} `json:"error"`
	Result interface{} `json:"result"`
}

func chopAndSwap(s string) []byte {
	b, err := hex.DecodeString(s[:160])
	if err != nil {
		panic(err)
	}
	for i := 0; i < len(b); i += 4 {
		b[i], b[i+1], b[i+2], b[i+3] = b[i+3], b[i+2], b[i+1], b[i]
	}
	return b
}

func sha256d(b []byte) []byte {
	a := sha256.Sum256(b)
	a = sha256.Sum256(a[:])
	return a[:]
}

func scryptHash(b []byte) []byte {
	r, _ := scrypt.Key(b, b, 1024, 1, 1, 32)
	return r
}

func reverse(b []byte) []byte {
	for i, j := 0, len(b)-1; i < j; i, j = i+1, j-1 {
		b[i], b[j] = b[j], b[i]
	}
	return b
}

type Work [80]byte

func NewWork(b []byte) *Work {
	if len(b) != 80 {
		panic(fmt.Errorf("incorrect number of bytes for Work: %d", len(b)))
	}
	var w Work
	for i := 0; i < len(w); i++ {
		w[i] = b[i]
	}
	return &w
}

// returns false if nonce overflowed
func (w *Work) incNonce() bool {
	for i := 79; i >= 76; i-- {
		w[i]++
		if w[i] != 0 {
			break
		}
		if i == 76 {
			return false
		}
	}
	return true
}

func (w *Work) nonce() []byte {
	return w[76:]
}

func getwork(pool, wallet string) (*Work, error) {
	b, err := json.Marshal(Request{ID: 0, Method: "getwork", Params: []string{}})
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("POST", pool, bytes.NewReader(b))
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(wallet, "suchpassword")
	req.Header.Set("Content-Type", "application/json-rpc")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	jsonResp := &Response{}
	if err = json.NewDecoder(resp.Body).Decode(jsonResp); err != nil {
		return nil, err
	}
	fmt.Println(jsonResp)
	resp.Body.Close()
	return NewWork(chopAndSwap(jsonResp.Result.(map[string]interface{})["data"].(string))), nil
}

func decodeOrDie(s string) []byte {
	b, err := hex.DecodeString(s)
	if err != nil {
		panic(err)
	}
	return b
}

func getHeader(version, prevBlock, merkleRoot, bits string, time, nonce uint32) []byte {
	r := make([]byte, 0, 80)
	r = append(r, decodeOrDie(version)...)
	r = append(r, reverse(decodeOrDie(prevBlock))...)
	r = append(r, reverse(decodeOrDie(merkleRoot))...)
	buf := make([]byte, 4)
	binary.LittleEndian.PutUint32(buf, time)
	r = append(r, buf...)
	r = append(r, reverse(decodeOrDie(bits))...)
	binary.LittleEndian.PutUint32(buf, nonce)
	r = append(r, buf...)

	return r
}

func main() {
	w, err := getwork("http://p2pool.name:9555", "D7dodxkNKBJREpaxtUsJ2ZoK91MiV6fqHp")
	if err != nil {
		panic(err)
	}
	fmt.Println(hex.EncodeToString(w[:]))
	fmt.Println(hex.EncodeToString(w.nonce()))
	w.incNonce()
	w.incNonce()
	w.incNonce()
	fmt.Println(hex.EncodeToString(w.nonce()))
	for i := 0; i < 256; i++ {
		w.incNonce()
	}
	fmt.Println(hex.EncodeToString(w.nonce()))
}
